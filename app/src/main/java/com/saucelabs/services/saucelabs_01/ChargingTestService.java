package com.saucelabs.services.saucelabs_01;

import android.app.IntentService;
import android.content.Context;
import android.content.Intent;

import android.os.IBinder;
import android.util.Log;
import android.widget.Toast;


import java.lang.reflect.Method;
import java.util.Arrays;


public class ChargingTestService extends IntentService {


    String TAG = "ChargingTestService";
    Context context;

    /**
     * A constructor is required, and must call the super constructor with a name for the worker thread.
     */
    public ChargingTestService() {
        super("ChargingTestService");

    }

    /**
     * The IntentService calls this method from the default worker thread with
     * the intent that started the service. When this method returns, IntentService
     * stops the service, as appropriate.
     */
    @Override
    protected void onHandleIntent(Intent intent) {

        // Using Reflection to get list of services available form ServicesManager

        try {

            Class localServiceManagerClass = Class.forName("android.os.ServiceManager");
            Method getService = localServiceManagerClass.getMethod("listServices");
            if(getService != null) {
                String[] servicesList = (String[]) getService.invoke(localServiceManagerClass);
                if(servicesList != null) {

                    for (int i = 0; i < servicesList.length; i++) {
                        Log.d("Serviceslist","Service Number: " + i + " Name: " + servicesList[i]);

                    }

                }
                else
                {
                    Log.e(TAG,"Error in getting servicesList");
                }

            }
            else
            {
                Log.e(TAG,"Problem in accessing Method listServices");
            }

        }
        catch (Exception e)
        {
            e.printStackTrace();
            Log.d(TAG,e.toString());

        }


        // Using Reflection to get batterystats service available form ServicesManager

        try {
            Class localServiceManagerClass = Class.forName("android.os.ServiceManager");
            Method getService = localServiceManagerClass.getMethod("getService", new Class[] {String.class});


            if(getService != null) {
                Object result = getService.invoke(localServiceManagerClass, new Object[]{"batterystats"});

                if(result != null) {

                    // Could not get instantiate com.android.internal.app.IBatteryStats because it is an interface
                    //Not sure what to do here ?

                    Log.d(TAG,"batterystats service is available");


                }
                else
                {
                    Log.d(TAG,"batterystats service is not available");
                }

            }
            else
            {

                Log.d(TAG,"Could not get Services from ServiceManager");

            }
        }
        catch (Exception e)
        {
            e.printStackTrace();
            Log.d(TAG,e.toString());

        }

        // Could not get instantiate com.android.internal.app.IBatteryStats because it is an interface,
        // Instead we use com.android.internal.os.BatteryStatsImpl to get isCharging status

        try {

            String BATTERY_STATS_IMPL_CLASS = "com.android.internal.os.BatteryStatsImpl";
            Object mBatteryStatsImpl = Class.forName(BATTERY_STATS_IMPL_CLASS).getConstructor().newInstance();
            Method isChargingMethod = Class.forName(BATTERY_STATS_IMPL_CLASS).getMethod("isCharging");

            boolean isChargingValue = (boolean)isChargingMethod.invoke(mBatteryStatsImpl);
            Log.d(TAG,"isCharging Value = "+ isChargingValue);
        }
        catch (Exception e)
        {
            e.printStackTrace();
            Log.d(TAG,e.toString());

        }


    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Toast.makeText(this, "Starting ChargingTestService", Toast.LENGTH_SHORT).show();
        Log.d(TAG,"Starting ChargingTestService");
        return super.onStartCommand(intent,flags,startId);
    }

    @Override
    public void onDestroy() {
        Toast.makeText(this, "ChargingTestService Done", Toast.LENGTH_SHORT).show();
        Log.d(TAG,"ChargingTestService Done");
    }
}
